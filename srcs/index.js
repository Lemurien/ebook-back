const Server = require("qpi")
const {Drivers, Engine} = require("ormon")
const config = require("./config")

Engine.init(new Drivers.Mongo(config.db.uri))

const server = new Server({cors: {origin: []}})

server.load(`${__dirname}/endpoints`)

if (process.env.NODE_ENV === "test") // eslint-disable-line no-process-env
  module.exports = server
else
  server.listen(8080, () => console.log("listening")) // eslint-disable-line no-console, no-magic-numbers, max-len
