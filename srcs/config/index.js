/* eslint-disable no-process-env */

module.exports = {
  app: {
    port: process.env.PORT,
  },
  db: {
    uri: process.env.MONGO_URI,
  },
}
