const {Engine, Model} = require("ormon")

class EBook extends Model {}

Engine.Bind("ebooks", EBook) // eslint-disable-line new-cap

module.exports = EBook
