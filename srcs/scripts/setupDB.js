const Driver = require("ormon").Drivers.Mongo
const config = require("../config")

/* eslint-disable no-console */

const driver = new Driver(config.db.uri)

const setVer = (ver, msg) => {
  console.log("               ", ver, "\t", msg)
  return ver
}

const getVer = async () => {
  let version = await driver.findOne("_versions", {}, {sort: [["version", -1]]}) // eslint-disable-line no-magic-numbers, max-len
  if (version)
    version = version.version // eslint-disable-line prefer-destructuring
  return version
}

const updateVer = async (version, latest) => {
  if (version === latest) {
    console.log("    up to date:", version)
  }
  else {
    await driver.insertOne("_versions", {date: new Date(), version})
    console.log("update version:", version)
  }
}

// eslint-disable-next-line max-statements
const run = async () => {
  try {
    let version = await getVer()
    const latest = version
    console.log("latest version:", version)

    if (version === null) {
      await driver.insertMany("ebooks", require("../../ebooks.json")) // eslint-disable-line global-require, max-len
      version = setVer("0.0.0")
    }

    await updateVer(version, latest)
  }
  catch (error) {
    console.log(error)
    process.exit(1) // eslint-disable-line no-magic-numbers, no-process-exit
  }
  finally {
    await driver.close()
  }
}

run().then(() => console.log("done"))
