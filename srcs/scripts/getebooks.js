const puppeteer = require("puppeteer")
const Driver = require("ormon").Drivers.Mongo
const config = require("../config")
const bookingUrl = "http://www.allitebooks.org/"

const getebook = async (browser, url) => {
  try {
    const page = await browser.newPage()
    await page.goto(url)
    // eslint-disable-next-line camelcase
    const [title, img_url, desc, meta, links] = await Promise.all([
      page.evaluate(() => document.querySelector(".single-title").innerText), // eslint-disable-line max-len, no-magic-numbers, max-statements-per-line, array-element-newline
      page.evaluate(() => document.querySelector(".attachment-post-thumbnail").src), // eslint-disable-line max-len, no-magic-numbers, max-statements-per-line, array-element-newline
      page.evaluate(() => [...document.querySelectorAll(".entry-content div, .entry-content p")].reduce((acc, val) => acc + val.innerText, "")), // eslint-disable-line max-len, no-magic-numbers, max-statements-per-line, array-element-newline
      page.evaluate(() => [...document.querySelector(".book-detail dl").childNodes].filter(a => a.tagName === "DD" || a.tagName === "DT").reduce((acc, val, idx, arr) => { if (val.tagName === "DT") return acc; acc[arr[idx - 1].innerText.slice(0, -1)] = val.innerText; return acc }, {})), // eslint-disable-line max-len, no-magic-numbers, max-statements-per-line, array-element-newline, array-element-newline, max-params
      page.evaluate(() => [...document.querySelectorAll(".download-links a")].map(a => { const res = {}; const lnk = a.href.split("."); res[lnk.slice(-1)] = a.href; return res }).reduce((acc, val) => Object.assign(acc, val), {})), // eslint-disable-line max-len, no-magic-numbers, array-element-newline, max-statements-per-line
    ])
    await page.close()
    // eslint-disable-next-line camelcase
    return {desc, img_url, links, meta, title}
  }
  catch (e) {
    return null
  }
}

const getpage = async (browser, url) => {
  const page = await browser.newPage()
  await page.goto(url)
  // eslint-disable-next-line max-len
  const links = await page.evaluate(() => [...document.querySelectorAll(".entry-title a")].map(a => a.href.slice()))
  const promises = []
  for (const item of links)
    promises.push(getebook(browser, item))
  const data = await Promise.all(promises)
  await page.close()
  return data.filter(a => Boolean(a))
}

const started = Date.now();

// eslint-disable-next-line max-statements
(async () => {
  const browser = await puppeteer.launch({args: ["--no-sandbox"], headless: true}) // eslint-disable-line max-len
  const page = await browser.newPage()
  await page.setViewport({height: 926, width: 1920})
  await page.goto(bookingUrl)
  const driver = new Driver(config.db.uri)
  // eslint-disable-next-line no-magic-numbers, max-len
  const max = await page.evaluate(() => Number(document.querySelector(".pagination").childNodes[15].innerText))
  const promises = [getpage(browser, bookingUrl)]

  for (let i = 2; i <= max; i++) {
    promises.push((async () => {
      const ebooks = await getpage(browser, `${bookingUrl}page/${i}/`)
      await driver.insertMany("ebooks", ebooks)
    })())
    if (i % 5 !== 0) continue // eslint-disable-line no-magic-numbers
    await Promise.all(promises) // eslint-disable-line no-await-in-loop
    const elapsed = Date.now() - started

    // eslint-disable-next-line no-magic-numbers, max-len, no-console
    console.log(`${i}/${max} [${Math.round(i / max * 100)}%]\t in ${elapsed}ms -> ${Math.round(i * elapsed / max / 1000)}s remaining`)
  }
  await Promise.all(promises)
})().then(() => process.exit(0)) // eslint-disable-line no-magic-numbers, no-process-exit, max-len
